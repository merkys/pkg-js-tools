# pkg-js/no-testsuite -- lintian check script for detecting a bad debian/watch configuration

package Lintian::pkg_js::repo;

use strict;
use warnings;
use Dpkg::IPC;
use Lintian::Tags qw(tag);

sub run {
    my ( $pkg, $type, $info, $proc, $group ) = @_;
    my ( $out, $err );
    return unless ( -e 'package.json' );
    spawn(
        exec            => ['debcheck-node-repo'],
        to_string       => \$out,
        error_to_string => \$err,
        wait_child      => 1,
        nocheck         => 1
    );
    tag 'inconsistency-debian-watch' if $?;
}

1;
